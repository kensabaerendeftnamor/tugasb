# No.1
![](https://gitlab.com/tugaskensa/tugasb/-/raw/main/dokumentasi/usecase2.jpg)
![](https://gitlab.com/tugaskensa/tugasb/-/raw/main/dokumentasi/usecase3.jpg)
![](https://gitlab.com/tugaskensa/tugasb/-/raw/main/dokumentasi/usecase4.jpg)

# No.2
![](https://gitlab.com/tugaskensa/tugasb/-/raw/main/dokumentasi/classdiagramcookpad.drawio.png)

# No.3
![]()

# No.4
![](https://gitlab.com/kensabaerendeftnamor/tugasb/-/raw/main/dokumentasi/Singleton.gif)

# No.5
![](https://gitlab.com/kensabaerendeftnamor/tugasb/-/raw/main/dokumentasi/Database.gif)

# No.6
![](https://gitlab.com/kensabaerendeftnamor/tugasb/-/raw/main/dokumentasi/CRUD.gif)

# No.7
![](https://gitlab.com/kensabaerendeftnamor/tugasb/-/raw/main/dokumentasi/GUI.gif)

# No.8
![](https://gitlab.com/kensabaerendeftnamor/tugasb/-/raw/main/dokumentasi/HTTP.gif)

# No.9
demonstrasi prduk digitla kepada publik

link youtube:
![](https://youtu.be/ZWouO6fidLs)

# No.10
untuk pengembangan produk digital yang saya buat kedepannya akan melibatkan penggunaan machine learning pada produk digital ini
